### Project introduction

This is an example project, containing a simple function with bugs. 

[Fuzz testing](https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/) usually is very efficient to detect bugs like this.
> **`!important` This project is prepared for [Javafuzz](https://gitlab.com/gitlab-org/security-products/analyzers/fuzzers/javafuzz) instead of [JQF Java](https://github.com/rohanpadhye/JQF)**

### Understand the bug introduction

The bug is located at `ParseComplex.Java` in the following code

```java
package org.gitlab.examplejava;

public class ParseComplex {
    public static boolean parse(String data) {
        if (data.length() > 4) {
            return false;
        }
        return data.charAt(0) == 'F' &&
                data.charAt(1) == 'U' &&
                data.charAt(2) == 'Z' &&
                data.charAt(3) == 'Z';
    }
}
```

This is a VERY simple case for the sake of the example the author made a mistake.
and Instead of `data.length() > 4 ` the correct code should be `data.length() < 4`.

### Javafuzz introduction

Fuzzing for Java can help find both complex bugs and correctness bugs. Java is a safe language so memory corruption bugs
are unlikely to happen, but bugs can still have security implications and
cause the app to fail in production.

### Building and running a javafuzz target

```
  script:
    - mvn install
    - ./gitlab-cov-fuzz run --regression=$REGRESSION --engine javafuzz -- com.gitlab.javafuzz.examples.FuzzParseComplex
```
 

